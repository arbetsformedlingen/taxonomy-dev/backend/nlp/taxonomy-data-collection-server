{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators   #-}
module Lib
    ( startApp
    , app
    ) where

import Control.Monad.IO.Class (liftIO)
import Data.Aeson
import Data.Aeson.TH
import Data.Semigroup ((<>))
import Network.Wai (Middleware)
import Network.Wai.Middleware.Cors (cors, simpleCorsResourcePolicy, corsMethods, corsRequestHeaders)
import Network.Wai.Handler.Warp (runSettings, setPort, setLogger, defaultSettings)
import Network.Wai.Logger (withStdoutLogger)
import Options.Applicative
import Servant

configDefaultPort :: Int
configDefaultPort = 8080

data Opts = Opts
  { debug :: Bool
  , port  :: Int
  }

data FeedbackItems = FeedbackItems
  { concept :: String
  , correct :: Bool
  } deriving (Eq)

data Feedback = Feedback
  { context :: String
  , feedback :: [FeedbackItems]
  } deriving (Eq)

instance Show FeedbackItems where
  show (FeedbackItems c f) = "{concept=" ++ show c ++ ", correct=" ++ show f ++ "}"

instance Show Feedback where
  show (Feedback c f) = "{context=" ++ show c ++ ", feedback=" ++ show f ++ "}"

$(deriveJSON defaultOptions ''FeedbackItems)
$(deriveJSON defaultOptions ''Feedback)

type API =
    "feedback" :> ReqBody '[JSON] Feedback :> Post '[JSON] NoContent

-- type Middleware = Application -> Application
corsPolicy :: Middleware
corsPolicy = cors (const $ Just policy)
    where
        policy = simpleCorsResourcePolicy
          { corsMethods = [ "GET", "POST", "PUT", "OPTIONS" ]
          , corsRequestHeaders = [ "content-type" ]
          }

options :: Parser Opts
options = Opts
    <$> switch
        ( long "debug"
        <> short 'd'
        <> help "Enable logging"
        )
    <*> option auto
        ( long "port"
        <> short 'p'
        <> help "Select which port to start on"
        <> showDefault
        <> value configDefaultPort
        <> metavar "PORT"
        )

startApp :: IO ()
startApp = start =<< execParser opts
  where
    opts = info (options <**> helper)
        ( fullDesc
        <> progDesc "Start the data collection server"
        <> header "JobTech Taxonomy - ML Data Collection Server"
        )

start :: Opts -> IO ()
start (Opts dbg port) = do
    liftIO $ putStrLn $ "Starting server on port " ++ show port
    withStdoutLogger $ \logger ->
        let log = if dbg then setLogger logger else id
        in runSettings (setPort port $ log $ defaultSettings) $ corsPolicy $ app

app :: Application
app = serve api server

api :: Proxy API
api = Proxy

feedbackH :: Feedback -> Handler NoContent
feedbackH json = do
    liftIO $ print json
    return NoContent

server :: Server API
server = feedbackH
