# Taxonomy ML Data Collection Server
A prototype server that collects data for machine-learning algorithms.

## Build

```
stack build
```
## Run

```
stack exec taxonomy-data-collection-server
```

## Test

```
curl http://localhost:8080/users
```
